import 'package:clima/screens/location_screen.dart';
import 'package:clima/screens/week_forecast_screen.dart';
import 'package:clima/services/weather.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ForecastLoadingScreen extends StatefulWidget {
  @override
  _ForecastLoadingScreenState createState() => _ForecastLoadingScreenState();
}

class _ForecastLoadingScreenState extends State<ForecastLoadingScreen> {
  @override
  void initState() {
    super.initState();
    getForecastData();
  }

  void getForecastData() async {
    var weatherData = await WeatherModel().getForecastWeather();
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return WeekForecastScreen(forecastWeather: weatherData);
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SpinKitDoubleBounce(
          color: Colors.white,
          size: 100,
        ),
      ),
    );
  }
}
