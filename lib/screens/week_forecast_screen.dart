import 'package:clima/services/weather.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class WeekForecastScreen extends StatefulWidget {
  WeekForecastScreen({this.forecastWeather});
  final forecastWeather;
  @override
  _WeekForecastScreenState createState() => _WeekForecastScreenState();
}

class _WeekForecastScreenState extends State<WeekForecastScreen> {
  List<dynamic> daily;
  WeatherModel weather = WeatherModel();
  @override
  void initState() {
    super.initState();
    updateUI(widget.forecastWeather);
  }

  void updateUI(weatherData) {
    daily = weatherData['daily'];
  }

  String getMessageWeather(double temp) {
    int temperature = temp.toInt();
    return weather.getMessage(temperature);
  }

  String getDateTime(time) {
    var date = new DateTime.fromMillisecondsSinceEpoch(time * 1000);
    return new DateFormat.yMMMMd('en_US').format(date);
  }

  int getTemp(double temp) {
    return temp.toInt();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/city_background.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        constraints: BoxConstraints.expand(),
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    size: 50.0,
                  ),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    return Card(
                      child: ListTile(
                        title: Text(getDateTime(daily[index]['dt'])),
                        leading: Text(
                          weather
                              .getWeatherIcon(daily[index]['weather'][0]['id']),
                          textScaleFactor: 3,
                        ),
                        subtitle: Text(
                          getMessageWeather(daily[index]['temp']['day']),
                        ),
                        trailing:
                            Text('${getTemp(daily[index]['temp']['day'])}°'),
                      ),
                    );
                  },
                  itemCount: daily.length,
                ),
              )
            ],
          ),
        ),
      ),
    );
    ;
  }
}
